-- run it with :
-- pig  -Dpig.additional.jars="../target/lib/*" -x local sample.pig
register ../target/zig-0.1-SNAPSHOT.jar ;
DEFINE my_add org.zoomba.zig.Int('@0 + @1');
rows = load 'data.csv' using PigStorage(',') as (tg:int, title:chararray, cid:chararray, ts:int );
e = FOREACH rows GENERATE tg, my_add(tg, ts);
dump e;
