package org.zoomba.zig;
import org.apache.pig.data.Tuple;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ZigTest{

    @Test
    public void safeCallWithException() throws Exception{
        final String addition = "@0 + @1";
        Zig zig = new Zig(addition,null);
        try {
            zig.exec(new TupleImpl(1));
            assertTrue(false);
        }catch (Throwable te){
            assertTrue( te instanceof RuntimeException );
        }
    }

    @Test
    public void testInt() throws Exception{
        final String addition = "@0 + @1";
        Int i = new Int(addition);
        assertEquals(3L,(long)i.exec( new TupleImpl(1,2)));
    }

    @Test
    public void testReal() throws Exception{
        final String addition = "@0 + @1";
        Real real = new Real(addition);
        assertEquals(3.0,real.exec( new TupleImpl(1.0,2.0)),0.01);
    }

    @Test
    public void testBoolean() throws Exception {
        Bool bool = new Bool( "@0 < @1 ;" );
        assertTrue(bool.exec( new TupleImpl(1,2)));
        assertFalse(bool.exec( new TupleImpl(3,2)));
    }

    @Test
    public void testString() throws Exception {
        Str str = new Str( " str(@0,@1) ;" );
        assertEquals("10", str.exec( new TupleImpl(2,2)));
    }

    @Test
    public void testTuple() throws Exception {
        Transform transform = new Transform( " $.logger.info('Cool!') ; [@0,@2] ;" );
        Tuple t = transform.exec( new TupleImpl(1,2,3));
        assertEquals(2,t.size());
        assertEquals(1,t.get(0));
        assertEquals(3,t.get(1));
    }
}