package org.zoomba.zig;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.types.ZNumber;

import java.io.IOException;

/**
 */
public class Real extends EvalFunc<Double>{

    private final Zig zig;

    public Real(String arg){
        zig = new Zig(arg,this);
    }

    @Override
    public Double exec(Tuple tuple) throws IOException {
        Number d = ZNumber.integer(zig.exec(tuple),null);
        if ( d == null ){
            throw new RuntimeException("Can not cast to double!");
        }
        return d.doubleValue();
    }
}
