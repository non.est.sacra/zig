package org.zoomba.zig;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.types.ZTypes;

import java.io.IOException;

/**
 */
public class Bool extends EvalFunc<Boolean>{

    private final Zig zig;

    public Bool(String arg){
        zig = new Zig(arg,this);
    }

    @Override
    public Boolean exec(Tuple tuple) throws IOException {
        Boolean b = ZTypes.bool(zig.exec(tuple),null);
        if ( b == null ){
            throw new RuntimeException("Can not cast to boolean!");
        }
        return b;
    }
}
