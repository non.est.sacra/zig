package org.zoomba.zig;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import java.io.IOException;

/**
 */
public class Str extends EvalFunc<String>{

    private final Zig zig;

    public Str(String arg){
        zig = new Zig(arg,this);
    }

    @Override
    public String exec(Tuple tuple) throws IOException {
        Object o = zig.exec(tuple);
        return String.valueOf(o);
    }
}
