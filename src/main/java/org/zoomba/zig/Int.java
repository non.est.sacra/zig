package org.zoomba.zig;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.types.ZNumber;

import java.io.IOException;

public class Int extends EvalFunc<Long> {

    private final Zig zig;

    public Int(String arg){
        zig = new Zig(arg,this);
    }

    @Override
    public Long exec(Tuple tuple) throws IOException {
        Number i = ZNumber.integer(zig.exec(tuple),null);
        if ( i == null ){
            throw new RuntimeException("Can not cast to integer!");
        }
        return i.longValue();
    }
}
