package org.zoomba.zig;

import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.collections.ZArray;
import zoomba.lang.core.types.ZTypes;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 */
public class TupleImpl implements Tuple{

    private final ZArray internal;

    public TupleImpl(Object...args){
        internal = new ZArray(args,false);
    }

    public TupleImpl(List l){
        internal = new ZArray(l.toArray(),false);
    }

    @Override
    public void reference(Tuple tuple) {

    }

    @Override
    public int size() {
        return internal.size();
    }

    @Override
    public boolean isNull(int i) throws ExecException {
        return null == internal.get(i) ;
    }

    @Override
    public byte getType(int i) throws ExecException {
        return -1; // don't care
    }

    @Override
    public Object get(int i) throws ExecException {
        return internal.get(i);
    }

    @Override
    public List<Object> getAll() {
        return internal;
    }

    @Override
    public void set(int i, Object o) throws ExecException {
        internal.set(i,o);
    }

    @Override
    public void append(Object o) {
        // no, not possible
    }

    @Override
    public long getMemorySize() {
        return 0; // no, not necessary
    }

    @Override
    public String toDelimitedString(String s) throws ExecException {
        return ZTypes.string(internal,s);
    }

    @Override
    public int compareTo(Object o) {
        return internal.compareTo(o);
    }

    @Override
    public Iterator<Object> iterator() {
        return internal.iterator();
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {

    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {

    }
}
