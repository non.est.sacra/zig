package org.zoomba.zig;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.interpreter.ZContext;
import zoomba.lang.core.interpreter.ZScript;
import zoomba.lang.core.operations.Function;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Zig {

    private static final String THIS = "$" ;

    private static final String ARG = "@" ;

    private final ZScript script;

    private final EvalFunc underlying;

    private static String text(String path) throws Exception{
        Path pt=new Path(path);
        FileSystem fs = FileSystem.get(new Configuration());
        BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(pt)));
        StringBuilder text = new StringBuilder();
        String line;
        line=br.readLine();
        while (line != null){
            text.append(line).append("\n");
            line=br.readLine();
        }
        return text.toString();
    }

    public Zig(String body, EvalFunc actual){
        if ( body == null ) throw new IllegalArgumentException("Body can not be null!");
        underlying = actual;
        if ( body.startsWith("//")){
           String path = body.substring(2);
           try {
               script = new ZScript( text(path));
           }catch (Exception e){
               throw new RuntimeException("Can not read from Path ");
           }
        } else {
            script = new ZScript(body);
        }
    }

    private Object run(Tuple tuple){
        ZContext.FunctionContext context = new ZContext.FunctionContext(
                ZContext.EMPTY_CONTEXT, args(tuple));
        context.set(THIS, underlying );
        script.setExternalContext(context);
        Function.MonadicContainer r = script.execute();
        if ( r.value() instanceof Throwable ){
            if ( r.value() instanceof RuntimeException ){
                throw (RuntimeException)r.value();
            }
            throw new RuntimeException((Throwable)r.value());
        }
        return r.value();
    }

    private static ZContext.ArgContext args(Tuple t) {
        try {
            List<String> names = new ArrayList<>();
            List<Function.MonadicContainer> values = new ArrayList<>();
            for ( int i = 0 ; i < t.size(); i++ ){
                names.add( ARG + Integer.toString(i));
                values.add( new Function.MonadicContainerBase(t.get(i)));
            }
            return new ZContext.ArgContext(names,values);

        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public Object exec(Tuple tuple) throws IOException {
        return run(tuple);
    }
}