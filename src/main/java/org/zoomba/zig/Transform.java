package org.zoomba.zig;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.Tuple;
import zoomba.lang.core.collections.ZArray;

import java.io.IOException;
import java.util.List;

/**
 */
public class Transform extends EvalFunc<Tuple> {

    private final Zig zig;

    public Transform(String args){
        zig = new Zig(args,this);
    }

    @Override
    public Tuple exec(Tuple tuple) throws IOException {
        Object o = zig.exec(tuple);
        if ( o == null ) { throw new RuntimeException("Result is null!"); }
        if ( o.getClass().isArray() ){
            return new TupleImpl(new ZArray(o));
        }
        if ( o instanceof List ){
            return new TupleImpl((List)o);
        }
        throw new RuntimeException("Result can not be put as a Tuple!");
    }
}
